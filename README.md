# git-notes

## Add

add a folder:

git add <folder_name>/*

## Commit

git commit -m "commit_message"

## Remove folder from index (useful when adding things to .gitignore)

git rm --cached -r .idea

## Fetching a Specific Branch

git fetch <remote_name> <remote_branch_name>

e.g. git fetch origin release/1.4.5

## Switching to a Remote Branch (by creating a local branch and checking out 
the remote branch to it, the newly created branch will automatically track the remote one)

git checkout -b <new_local_branch_name> <remote_name>/<remote_branch_name>
e.g. git checkout -b release/1.4.5 origin/release/1.4.5

## Pushing to a remote branch

git push <remote> <local_branch_name>:<remote_branch_name>

By default the remote is origin and you specify the name of the remote branch next omitting local. But defaults are subject to configuration and change from version
to version so easiest thing is to use the previous format.

e.g. git push origin payment-team-reps:feature/payment-team-reports

## Show what Local Branches are Tracking what Remote Branches

git branch -vv

## Find out from what Branch was a Branch Branched from

git log --graph --decorate --oneline

## Revert one commit

This will leave the working directory intact but will go back one commit:

git reset HEAD~

git add <the changes you want to keep>

git commit everything you want from previous commit (or don't do this and previous step at all...)

git push -f ... !!! If no one else has checked out your branch ONLY !!!

git reset --hard HEAD ... (in order to make your working directory the same as the last commit)

## Delete remote branch

git push origin --delete bugfix/GTM-1017-make-unit-integration-tests-platform-idependent
